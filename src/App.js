import React from 'react';
import { PrimaryButton } from 'office-ui-fabric-react';
import logo from './logo.svg';

function App() {
  return (
    <PrimaryButton>I am a button.</PrimaryButton>
  );
}

export default App;
